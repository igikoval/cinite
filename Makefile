# -- main configuration
#
# packages that will be compiled
PACKS   = core index
#
# drivers that will be compiled
DRIVERS = file


OUTFILE = cinite    # output file name
TESTOUTFILE = test  # test output file name

CC     = g++
CFLAGS = -std=c++14 -DLINUX -Wall
LIBS   =
MAKEFLAGS  = -j 5

DOXYGEN    = doxygen
DOXYGENCFG = ./doxygen.conf
DOCDIR     = doc/

RELFLAGS = -O2
DBGFLAGS = -Og -g

SRCDIR  = src
OUTDIR  = out

SUBDIRS = ./ system/ system/core/ system/core/abstract/ system/core/registry/
INCDIRS = ./ app/
PACKDIR = app/
PACKSDIRS  = models/ controllers/ views/ etc/ meta/ var/ lib/
TESTDIR = test/
DRIVERSDIR = system/core/driver/


FDIRS   := $(SUBDIRS) $(foreach pdir, $(PACKSDIRS), $(addsuffix /$(pdir), $(addprefix $(PACKDIR), $(PACKS))))

SOURCES := $(wildcard $(addprefix $(SRCDIR)/, $(addsuffix *.cpp, $(FDIRS)))) $(wildcard $(addsuffix .cpp, $(addprefix $(SRCDIR)/$(DRIVERSDIR), $(DRIVERS)))) $(wildcard $(addsuffix **/*.cpp, $(addprefix $(SRCDIR)/$(DRIVERSDIR), $(DRIVERS))))

INCPATHS := $(addprefix -I $(SRCDIR)/, $(INCDIRS))

OBJECTS := $(patsubst $(SRCDIR)/%.cpp,$(OUTDIR)/%.o,$(SOURCES))
LIBS    := $(addprefix -l,$(LIBS))


$(OUTDIR)/%.o: $(SRCDIR)/%.cpp
	@fdir="$(strip $(patsubst $(OUTDIR)/%.o, $(OUTDIR)/$(OUTSUBDIR)/%.o, $@))"; \
	touched=0; \
	if [ ! -f $$fdir ]; then \
		mkdir -p $(dir $(strip $(patsubst $(OUTDIR)/%.o, $(OUTDIR)/$(OUTSUBDIR)/%.o, $@))); \
	 	touch $$fdir; \
		touched=1; fi; \
	if [ $$touched -eq 1 ] || [ `date +%s -r $$fdir` -lt `date +%s -r $<` ]; then \
		echo "$(CC) -c -o $$fdir $< $(CFLAGS) $(INCPATHS)"; \
		$(CC) -c -o $$fdir $< $(CFLAGS) $(INCPATHS); fi



# -- release - target
release: CFLAGS += $(RELFLAGS)
release: OUTSUBDIR = rel
release: build

# -- debug - target
debug: CFLAGS += $(DBGFLAGS)
debug: OUTSUBDIR = dbg
debug: build

# -- finally, building
build: $(OBJECTS)
	$(CC) -o $(OUTDIR)/$(OUTSUBDIR)/$(OUTFILE) $(patsubst $(OUTDIR)/%.o, $(OUTDIR)/$(OUTSUBDIR)/%.o, $(OBJECTS)) $(CFLAGS) $(LIBS) $(INCPATHS)
	ln -fs $(OUTDIR)/$(OUTSUBDIR)/$(OUTFILE) $(strip $(OUTFILE))_$(strip $(OUTSUBDIR))

# -- generating documentation
.PHONY: doc
doc:
	$(DOXYGEN) $(DOXYGENCFG)

# -- unit testing section
TESTSUBDIRS := core/ core/stubs/ ./
TESTSOURCES := $(wildcard $(addprefix $(SRCDIR)/$(TESTDIR), $(addsuffix *.cpp, $(TESTSUBDIRS)))) $(filter-out %/main.cpp,$(SOURCES))
TESTOBJECTS := $(patsubst $(SRCDIR)/%.cpp,$(OUTDIR)/%.o,$(TESTSOURCES))

test: OUTSUBDIR = test
test: CFLAGS += $(DBGFLAGS)
test: $(TESTOBJECTS)
	$(CC) -o $(TESTOUTFILE) $(patsubst $(OUTDIR)/%.o, $(OUTDIR)/$(OUTSUBDIR)/%.o, $(TESTOBJECTS)) $(CFLAGS) $(LIBS) $(INCPATHS) -I src/test/

# -- cleaning
clean:
	rm -rf $(OUTDIR)
	rm -f $(TESTOUTFILE)
	rm -f $(strip $(OUTFILE))_*

clean_doc:
	rm -rf $(DOCDIR)
