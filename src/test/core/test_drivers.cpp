#include <iostream>
#include <fstream>
#include "config.hpp"
#include "stubs/loader.hpp"
#include "stubs/driver.hpp"
#include "stubs/view.hpp"
#include "system/core/driver/file.hpp"
#include "test/catch.hpp"

using namespace cinite::core::driver;

cinite::core::Loader load = cinite::core::Loader();

TEST_CASE("testing file driver") {

    cinite::instance<File> file = cinite::cast<File>(load.getDriver("File"));

    SECTION("reading from file") {
        std::string filepath = "readFromFileTest.txt";
        std::string fileText = "theTestFileText";

        std::ofstream testfile;
        testfile.open(filepath);
        testfile << fileText;
        testfile.close();

        REQUIRE(file->getFileContents(filepath) == fileText);
        remove(filepath.c_str());
    }

    SECTION("reading lines from file") {
        std::string filepath = "readLinesFromFileTest.txt";
        std::string fileText = "line1\nline2\nline3";
        std::vector<std::string> fileLines = {"line1", "line2", "line3"};

        std::ofstream testfile;
        testfile.open(filepath);
        testfile << fileText;
        testfile.close();

        std::vector<std::string> readedLines = file->getFileLines(filepath);
        int linesCount = readedLines.size();

        REQUIRE(linesCount == fileLines.size());
        for (int i = 0; i < linesCount; i++) {
            REQUIRE(readedLines[i] == fileLines[i]);
        }
        remove(filepath.c_str());
    }

    SECTION("writing string to file") {
        std::string filepath = "writeFileTest.txt";
        std::string fileText = "test\nasdf";

        file->putFileContents(filepath, fileText);

        REQUIRE(file->getFileContents(filepath) == fileText);
        remove(filepath.c_str());
    }
}
