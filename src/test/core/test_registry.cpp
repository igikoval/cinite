#include "config.hpp"
#include "system/core/registry.hpp"
#include "stubs/loader.hpp"
#include "stubs/model.hpp"
#include "stubs/controller.hpp"
#include "stubs/driver.hpp"
#include "stubs/view.hpp"
#include "test/catch.hpp"

using namespace cinite;
using namespace cinite::core;

TEST_CASE("getting registrations from registry class") {

    cinite::core::Loader load;

    SECTION("testing models with modelStub") {
        // testing model loading
        instance<modelStub> model = cast<modelStub>(Registry::getModelCreator("test", "modelStub")(load));

        REQUIRE(model->inited == false);
        model->__init();
        REQUIRE(model->inited == true);

        // testing exceptions on not registered model
        bool unregisteredModelFound = true;
        try {
            Registry::getModelCreator("test", "unregisterd_model");
        } catch (cinite::exception::registryException ex) {
            if (cinite::exception::registryException::MODEL_NOT_REGISTERED == ex.code)
                unregisteredModelFound = false;
        }

        REQUIRE(unregisteredModelFound == false);

        // testing exceptions on not registered pack
        bool unregisteredPackFound = true;
        try {
            Registry::getModelCreator("unregistered_pack", "unregisterd_model");
        } catch (cinite::exception::registryException ex) {
            if (cinite::exception::registryException::PACK_NOT_REGISTERED == ex.code)
                unregisteredPackFound = false;
        }

        REQUIRE(unregisteredPackFound == false);
    }


    SECTION("testing controllers with controllerStub") {
        // testing controller loading
        instance<controllerStub> ctrl = cast<controllerStub>(Registry::getControllerCreator("test", "controllerStub")(load));

        REQUIRE(ctrl->inited == false);
        ctrl->__init();
        REQUIRE(ctrl->inited == true);

        // testing exceptions on not registered controller
        bool unregisteredControllerFound = true;
        try {
            Registry::getControllerCreator("test", "unregistered_controller");
        } catch (cinite::exception::registryException ex) {
            if (cinite::exception::registryException::CONTROLLER_NOT_REGISTERED == ex.code)
                unregisteredControllerFound = false;
        }

        REQUIRE(unregisteredControllerFound == false);

        // testing exceptions on not registered pack
        bool unregisteredPackFound = true;
        try {
            Registry::getControllerCreator("unregistered_pack", "unregistered_controller");
        } catch (cinite::exception::registryException ex) {
            if (cinite::exception::registryException::PACK_NOT_REGISTERED == ex.code)
                unregisteredPackFound = false;
        }

        REQUIRE(unregisteredPackFound == false);
    }

    SECTION("testing views with viewStub") {
        // testing view loading
        instance<viewStub> view = cast<viewStub>(Registry::getViewCreator("test", "viewStub")(load));

        REQUIRE(view->getText() == "");
        view->__init();
        REQUIRE(view->getText() == viewStub::text);

        // testing exceptions on not registered view
        bool unregisteredViewFound = true;
        try {
            Registry::getViewCreator("test", "unregistered_view");
        } catch (cinite::exception::registryException ex) {
            if (cinite::exception::registryException::VIEW_NOT_REGISTERED == ex.code)
                unregisteredViewFound = false;
        }

        REQUIRE(unregisteredViewFound == false);

        // testing exceptions on not registered pack
        bool unregisteredPackFound = true;
        try {
            Registry::getViewCreator("unregistered_pack", "unregistered_view");
        } catch (cinite::exception::registryException ex) {
            if (cinite::exception::registryException::PACK_NOT_REGISTERED == ex.code)
                unregisteredPackFound = false;
        }

        REQUIRE(unregisteredPackFound == false);
    }

    SECTION("testing drivers with driverStub") {
        // testing driver loading
        instance<driverStub> drv = cast<driverStub>(Registry::getDriverCreator("core", "driverStub")(load));

        REQUIRE(drv->inited == false);
        drv->__init();
        REQUIRE(drv->inited == true);

        // testing exceptions on not registered view
        bool unregisteredDriverFound = true;
        try {
            Registry::getDriverCreator("core", "unregistered_driver");
        } catch (cinite::exception::registryException ex) {
            if (cinite::exception::registryException::DRIVER_NOT_REGISTERED == ex.code)
                unregisteredDriverFound = false;
        }

        REQUIRE(unregisteredDriverFound == false);

        // testing exceptions on not registered pack
        bool unregisteredPackFound = true;
        try {
            Registry::getDriverCreator("unregistered_pack", "unregistered_driver");
        } catch (cinite::exception::registryException ex) {
            if (cinite::exception::registryException::PACK_NOT_REGISTERED == ex.code)
                unregisteredPackFound = false;
        }

        REQUIRE(unregisteredPackFound == false);
    }
}
