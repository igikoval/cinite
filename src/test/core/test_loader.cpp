#include <iostream>
#include "config.hpp"
#include "stubs/loader.hpp"
#include "stubs/model.hpp"
#include "stubs/driver.hpp"
#include "stubs/view.hpp"
#include "system/exception/abstract.hpp"
#include "test/catch.hpp"

TEST_CASE("testing loader class") {

    cinite::core::Loader load;

    SECTION("testing translator") {
        std::pair<std::string, std::string> testname = load._translateName("asdf/asdf");
        std::string wholename = load._translateName("asdf", "asdf");
        REQUIRE(testname.first == "asdf");
        REQUIRE(testname.second == "asdf");
        REQUIRE(wholename == "asdf/asdf");
    }

    SECTION("testing basic loading") {
        cinite::instance<modelStub> mod = cinite::cast<modelStub>(load.getModel("test", "modelStub"));
        REQUIRE(mod->inited == true);

        cinite::instance<viewStub> view = cinite::cast<viewStub>(load.getView("test", "viewStub"));
        REQUIRE(view->getText() == viewStub::text);
    }

    SECTION("testing singletons reusing") {
        int start_count = load._singletons.size();
        cinite::instance<modelStub> sng = cinite::cast<modelStub>(load.getSingleton("test", "modelStub"));
        REQUIRE(load._singletons.size() == start_count + 1);

        cinite::instance<modelStub> sng2 = cinite::cast<modelStub>(load.getSingleton("test", "modelStub"));
        REQUIRE(load._singletons.size() == start_count + 1);
    }

    SECTION("testing drivers reusing") {
        int start_count = load._drivers.size();
        cinite::instance<driverStub> drv = cinite::cast<driverStub>(load.getDriver("driverStub"));
        REQUIRE(load._drivers.size() == start_count + 1);

        cinite::instance<driverStub> drv2 = cinite::cast<driverStub>(load.getDriver("driverStub"));
        REQUIRE(load._drivers.size() == start_count + 1);

        cinite::instance<driverStub> drv3 = cinite::cast<driverStub>(load.getDriver("driverStub", "alternative"));
        REQUIRE(load._drivers.size() == start_count + 2);

        cinite::instance<driverStub> drv4 = cinite::cast<driverStub>(load.getDriver("driverStub", "alternative"));
        REQUIRE(load._drivers.size() == start_count + 2);
    }

    SECTION("drivers and singletons frees") {
        cinite::instance<driverStub> drv = cinite::cast<driverStub>(load.getDriver("driverStub"));
        cinite::instance<modelStub> mod = cinite::cast<modelStub>(load.getSingleton("test", "modelStub"));

        int drivers_count = load._drivers.size();
        int singletons_count = load._singletons.size();

        load.freeDriver("driverStub");
        load.freeSingleton("test", "modelStub");

        REQUIRE(load._drivers.size() == drivers_count - 1);
        REQUIRE(load._singletons.size() == singletons_count - 1);
    }
}
