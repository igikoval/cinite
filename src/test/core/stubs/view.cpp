#include "view.hpp"

CINITE_REGISTER(viewStub, test, viewStub);

void viewStub::__init() {
    (*this) << viewStub::text;
}

const std::string viewStub::text = "asdf";
