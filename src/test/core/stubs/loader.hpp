#ifndef H_SYSTEM_CORE_LOADER
#define H_SYSTEM_CORE_LOADER

#include <string>
#include <map>
#include <memory>
#include "config.hpp"
#include "system/core/registry.hpp"
#include "system/core/config.hpp"


namespace cinite {
    namespace core {

        namespace abstract {
            class Abstract;
            class Model;
            class View;
            class Driver;
        }

        class Loader {
            public:
                Loader(){};
                ~Loader();


                instance<abstract::Model> getModel(std::string pack, std::string name);
                instance<abstract::View> getView(std::string pack, std::string name);
                instance<abstract::Model> getSingleton(std::string pack, std::string name);
                instance<abstract::Driver> getDriver(std::string name, std::string inst = "");
                void freeSingleton(std::string pack, std::string name);
                void freeDriver(std::string name, std::string inst = "");


                Config* getConfig() {
                    return &this->config;
                }

                std::pair<std::string, std::string> _translateName(std::string wholename);
                std::string _translateName(std::string pack, std::string name);

                std::map<std::string, instance<abstract::Model>> _singletons;
                std::map<std::string, instance<abstract::Driver>> _drivers;

            private:

                Config config = Config();
        };

    }
}

#endif
