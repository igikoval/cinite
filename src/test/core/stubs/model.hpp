#ifndef H_TEST_CORE_STUBS_MODEL
#define H_TEST_CORE_STUBS_MODEL

#include "system/core/registry.hpp"
#include "system/core/types/registerClass.hpp"
#include "system/core/abstract/model.hpp"
#include "system/core/macros.hpp"

class modelStub: public cinite::core::abstract::Model {
    public:
        CINITE_MODEL_CONSTRUCT();
        void __init() { this->inited = true; };
        bool inited = false;

    private:
        CINITE_DEFINE_REGISTRY(modelStub);

};

#endif
