#ifndef H_TEST_CORE_STUBS_VIEW
#define H_TEST_CORE_STUBS_VIEW

#include "system/core/registry.hpp"
#include "system/core/types/registerClass.hpp"
#include "system/core/abstract/view.hpp"
#include "system/core/macros.hpp"

class viewStub: public cinite::core::abstract::View {
    public:
        CINITE_VIEW_CONSTRUCT();

        static const std::string text;

        void __init();

    private:
        CINITE_DEFINE_REGISTRY(viewStub);

};

#endif
