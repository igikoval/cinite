#ifndef H_TEST_CORE_STUBS_DRIVER
#define H_TEST_CORE_STUBS_DRIVER

#include "system/core/registry.hpp"
#include "system/core/types/registerClass.hpp"
#include "system/core/abstract/driver.hpp"
#include "system/core/macros.hpp"

class driverStub: public cinite::core::abstract::Driver {
    public:
        CINITE_DRIVER_CONSTRUCT();
        void __init() { this->inited = true; };
        bool inited = false;

    private:
        CINITE_DEFINE_REGISTRY(driverStub);

};

#endif
