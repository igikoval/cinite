#ifndef H_TEST_CORE_STUBS_CONTROLLER
#define H_TEST_CORE_STUBS_CONTROLLER

#include "system/core/registry.hpp"
#include "system/core/types/registerClass.hpp"
#include "system/core/abstract/controller.hpp"
#include "system/core/macros.hpp"

class controllerStub: public cinite::core::abstract::Controller {
    public:
        CINITE_CONTROLLER_CONSTRUCT();
        void __init() { this->inited = true; };
        bool inited = false;

    private:
        CINITE_DEFINE_REGISTRY(controllerStub);

};

CINITE_REGISTER(controllerStub, test, controllerStub);

#endif
