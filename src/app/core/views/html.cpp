#include "system/core/driver/file.hpp"
#include "html.hpp"

using namespace cinite;
using namespace cinite::app::core::views;
using namespace cinite::core::driver;

CINITE_REGISTER(Html, core, html);

Html& Html::setFile(std::string filename) {
    std::string filepath = this->getConfig()->views_dir + filename;
    instance<File> file = this->getDriver<File>("file");
    (*this) << file->getFileContents(filepath);

    return (*this);
}
