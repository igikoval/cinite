#ifndef H_APP_CORE_VIEWS_HTML
#define H_APP_CORE_VIEWS_HTML

#include <string>
#include "system/core/cinite.hpp"

using namespace cinite::core;

namespace cinite {
namespace app {
namespace core {
    namespace views {

        /** Html file view */
        class Html: public abstract::View {
            public:
                /**
                 * Inherited view constructor
                 * @see abstract::View::View
                 */
                CINITE_VIEW_CONSTRUCT();

                /** initialization */
                void __init(){};

                /** Sets HTML file to be loaded */
                Html& setFile(std::string filename);

            private:
                CINITE_DEFINE_REGISTRY(Html);
        };

    }
}
}
}

#endif
