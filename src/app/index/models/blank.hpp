#ifndef H_APP_INDEX_MODELS_INDEX
#define H_APP_INDEX_MODELS_INDEX

#include <string>
#include "system/core/cinite.hpp"

using namespace cinite::core;

/** Blank base model */
class Index_Model_Blank: public abstract::Model {
    public:
        /**
         * Inherited model constructor
         * @see abstract::Model::Model
         */
        CINITE_MODEL_CONSTRUCT();

        /** initialization */
        void __init();

    private:
        CINITE_DEFINE_REGISTRY(Index_Model_Blank);
};

#endif
