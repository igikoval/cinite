#include "index.hpp"
#include "../models/blank.hpp"

CINITE_REGISTER(Index_Controller_Index, index, index);

void Index_Controller_Index::__init() {
    CINITE_ADD_METHOD(Index_Controller_Index, index);
}

void Index_Controller_Index::index(std::vector<std::string> arguments) {
    cinite::instance<Index_Model_Blank> mod_index = this->getModel<Index_Model_Blank>("index", "blank");
    printf("Index controller inited\n");

}
