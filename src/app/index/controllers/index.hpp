#ifndef H_APP_INDEX_CONTROLLERS_INDEX
#define H_APP_INDEX_CONTROLLERS_INDEX

#include <string>
#include "system/core/cinite.hpp"

using namespace cinite::core;

/** Blank base controller */
class Index_Controller_Index: public abstract::Controller {
    public:
        /**
         * Inherited controller constructor
         * @see abstract::Controller::Controller
         */
        CINITE_CONTROLLER_CONSTRUCT();

        /** initialization */
        void __init();

        /** Main method */
        void index(std::vector<std::string> arguments);

    private:
        CINITE_DEFINE_REGISTRY(Index_Controller_Index);
};

#endif
