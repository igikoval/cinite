#ifndef H_SYSTEM_EXCEPTION_COREEXCEPTION
#define H_SYSTEM_EXCEPTION_COREEXCEPTION

#include "abstract.hpp"

namespace cinite {
    namespace exception {

        /**
         * Core exception
         */
        class coreException: public Abstract {
            public:
                /**
                 * @see Abstract::Abstract
                 */
                coreException(unsigned int code, std::string message): Abstract(code, message) {};

                /** Pack was not registered or was broken */
                const static unsigned int NOT_VALID_PACK       = 1;

                /** Controller was not registered or was broken */
                const static unsigned int NOT_VALID_CONTROLLER = 2;

                /** Controller was not registered or was broken */
                const static unsigned int NOT_VALID_METHOD     = 3;

                /** Exception type: Core exception */
                const static unsigned int __exceptionType = Abstract::t_core;
        };
    }
}

#endif
