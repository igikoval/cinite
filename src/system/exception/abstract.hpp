#ifndef H_SYSTEM_EXCEPTION_EXCEPTION
#define H_SYSTEM_EXCEPTION_EXCEPTION

#include <string>

namespace cinite {

    /** Exceptions */
    namespace exception {

        /**
         * Basic exception class, every other will inherit from this one
         */
        class Abstract {
            public:
                /**
                 * Creates exception
                 * @param code    Exception code
                 * @param message Exception debug message
                 */
                Abstract(unsigned int code, std::string message) {
                    this->code    = code;

                    #if C_CINITE_SET_EXCEPTION_MESSAGES == 1
                    this->message = message;
                    #endif
                }

                /** exception's code          */
                unsigned int code   = 0;

                /** exception's debug message */
                std::string message = "";


                /** type: virtual, abstract exception */
                const static unsigned int t_abstract = 0;

                /** type: registry exception */
                const static unsigned int t_registry = 1;

                /** type: core exception */
                const static unsigned int t_core     = 2;

                /** Exception type: Abstract exception */
                const static unsigned int __exceptionType = Abstract::t_core;
        };
    }
}

#endif
