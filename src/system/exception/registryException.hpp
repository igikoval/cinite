#ifndef H_SYSTEM_EXCEPTION_LOADEREXCEPTION
#define H_SYSTEM_EXCEPTION_LOADEREXCEPTION

#include "abstract.hpp"

namespace cinite {
    namespace exception {

        namespace registry {}

        /**
         * Registry exception
         */
        class registryException: public Abstract {
            public:
                /**
                 * @see Abstract::Abstract
                 */
                registryException(unsigned int code, std::string message): Abstract(code, message) {};

                /** Pack is not registered */
                const static unsigned int PACK_NOT_REGISTERED       = 1;

                /** Controller is not registered */
                const static unsigned int CONTROLLER_NOT_REGISTERED = 2;

                /** Model is not registered */
                const static unsigned int MODEL_NOT_REGISTERED      = 3;

                /** View is not registered */
                const static unsigned int VIEW_NOT_REGISTERED       = 4;

                /** Driver is not registered */
                const static unsigned int DRIVER_NOT_REGISTERED     = 5;


                /** Exception type: Registry exception */
                const static unsigned int __exceptionType = Abstract::t_registry;
        };
    }
}

#endif
