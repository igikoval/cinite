#include "registry.hpp"
#include "registry/pack.hpp"

using namespace cinite::core;

Registry::~Registry() {
    std::map<std::string, registry::Pack*>::iterator it;

    for (it = this->_packs.begin(); it != this->_packs.end(); ++it) {
        delete it->second;
    }

    this->_packs.clear();
}

void Registry::registerAbstract(std::string pack, std::string name, abstractCreatorFunc creator, const unsigned int type) {
    Registry & reg = Registry::getInstance();
    reg.getPack(pack, true)->registerAbstract(name, creator, type);
}

abstractCreatorFunc Registry::getModelCreator(std::string pack, std::string name) {
    Registry &reg = Registry::getInstance();

    return reg.getPack(pack)->getModelCreator(name);
}

abstractCreatorFunc Registry::getControllerCreator(std::string pack, std::string name) {
    Registry &reg = Registry::getInstance();

    return reg.getPack(pack)->getControllerCreator(name);
}

abstractCreatorFunc Registry::getViewCreator(std::string pack, std::string name) {
    Registry &reg = Registry::getInstance();

    return reg.getPack(pack)->getViewCreator(name);
}

abstractCreatorFunc Registry::getDriverCreator(std::string pack, std::string name) {
    Registry &reg = Registry::getInstance();

    return reg.getPack(pack)->getDriverCreator(name);
}

registry::Pack *Registry::getPack(std::string name, bool create) {
    std::map<std::string, registry::Pack*>::iterator it;

    it = this->_packs.find(name);

    if (it == this->_packs.end()) {
        if (false == create) {
            throw exception::registryException(exception::registryException::PACK_NOT_REGISTERED, "Pack not found in registry (" + name + ")");
        }

        this->_packs[name] = new registry::Pack(name);
        return this->_packs[name];
    }

    return it->second;
}
