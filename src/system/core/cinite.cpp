#include <iostream>
#include "cinite.hpp"
#include "system/exception/registryException.hpp"
#include "system/exception/coreException.hpp"

using namespace cinite;
using namespace cinite::core;

Cinite::Cinite(): loader(Loader()) {
}

Cinite::Cinite(int argc, char * argv[]): loader(Loader()) {
    std::vector<std::string> arguments;

    arguments.reserve(argc - 1);


    for (int i = 1; i < argc; i++) {
        arguments.push_back(std::string(argv[i]));
    }

    this->run(arguments);
}

Cinite::Cinite(std::vector<std::string> arguments): loader(Loader()) {
    this->run(arguments);
}

void Cinite::run(std::vector<std::string> arguments) {
    std::string s_pack, s_ctrl, s_method;
    std::vector<std::string> args;
    instance<abstract::Controller> controller;

    unsigned int argc = arguments.size();

    s_pack   = (argc < 1) ? "index" : arguments[0];
    s_ctrl   = (argc < 2) ? "index" : arguments[1];
    s_method = (argc < 3) ? "index" : arguments[2];


    if (argc > 4) {
        args.reserve(argc - 3);
        for (unsigned int i = 4; i < argc; i++) {
            args.push_back(arguments[i]);
        }
    }

    try {
        abstractCreatorFunc creator = Registry::getControllerCreator(s_pack, s_ctrl);
        controller = cast<abstract::Controller>(creator(this->loader));
        controller->__init();

    } catch (exception::registryException ex) {
        switch (ex.code) {
            case exception::registryException::PACK_NOT_REGISTERED:
                throw exception::coreException(exception::coreException::NOT_VALID_PACK, "Pack not found (" + s_pack + ")");
                break;

            case exception::registryException::CONTROLLER_NOT_REGISTERED:
                throw exception::coreException(exception::coreException::NOT_VALID_CONTROLLER, "Controller not found (" + s_ctrl + ")");
                break;

            default:
                throw ex;
        }
    }

    abstract::Controller::method method = controller->getMethod(s_method);
    (&(*controller)->*method)(args);
}
