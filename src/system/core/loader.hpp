#ifndef H_SYSTEM_CORE_LOADER
#define H_SYSTEM_CORE_LOADER

#include <string>
#include <map>
#include <memory>
#include "config.hpp"
#include "registry.hpp"
#include "system/core/config.hpp"



namespace cinite {
    namespace core {

        namespace abstract {
            class Abstract;
            class Model;
            class View;
            class Driver;
        }

        /**
         * This one loads everything for models, controllers, etc
         */
        class Loader {
            public:
                Loader(){};
                ~Loader(){};

                /**
                 * Creates and inits a model, then returns a pointer
                 *
                 * @param  pack Source pack of model
                 * @param  name Model name
                 *
                 * @throw cinite::exception::registryException::PACK_NOT_REGISTERED
                 *          when pack was not found
                 * @throw cinite::exception::registryException::MODEL_NOT_REGISTERED
                 *          when model was not found
                 *
                 * @return      Created model pointer
                 */
                instance<abstract::Model> getModel(std::string pack, std::string name);

                /**
                 * Creates view object, then returns a pointer
                 *
                 * @param  pack Source pack of view
                 * @param  name View name
                 *
                 * @throw cinite::exception::registryException::PACK_NOT_REGISTERED
                 *          when pack was not found
                 * @throw cinite::exception::registryException::VIEW_NOT_REGISTERED
                 *          when model was not found
                 *
                 * @return      Created view pointer
                 */
                instance<abstract::View> getView(std::string pack, std::string name);

                /**
                 * Returns singleton pointer
                 *  Singleton is just a loader-unique model.
                 *
                 * @see Loader::getModel
                 */
                instance<abstract::Model> getSingleton(std::string pack, std::string name);

                /**
                 * Returns driver pointer
                 *  Driver is allways a singleton
                 *  There is no need for pakck name as drivers are allways core
                 *
                 * @param name Driver name
                 * @param inst Instance name - this let you create separate
                 *              istances of one driver with this string as
                 *              identifier.
                 *
                 * @throw cinite::exception::registryException::PACK_NOT_REGISTERED
                 *          when pack was not found
                 * @throw cinite::exception::registryException::DRIVER_NOT_REGISTERED
                 *          when model was not found
                 *
                 * @return      Created driver pointer
                 */
                instance<abstract::Driver> getDriver(std::string name, std::string inst = "");

                /**
                 * Frees singleton from loader
                 *
                 * @see Loader::getSingleton
                 */
                void freeSingleton(std::string pack, std::string name);

                /**
                 * Frees driver from loader
                 *
                 * @see Loader::freeDriver
                 */
                void freeDriver(std::string name, std::string inst = "");


                /** Returns config object */
                Config* getConfig() {
                    return &this->config;
                }

            private:
                /**
                 * Translates internal name for a pack/name pair
                 * @param wholename Internal name
                 */
                std::pair<std::string, std::string> _translateName(std::string wholename);

                /**
                 * Translates pack/name for a internal name
                 * @return Internal name
                 */
                std::string _translateName(std::string pack, std::string name);

                /** Loaded singletons */
                std::map<std::string, instance<abstract::Model>> _singletons;

                /** Loaded drivers */
                std::map<std::string, instance<abstract::Driver>> _drivers;

                /** App's config */
                Config config = Config();
        };

    }
}

#endif
