#ifndef H_SYSTEM_CORE_REGISTRY
#define H_SYSTEM_CORE_REGISTRY

#include <map>
#include <string>
#include "macros.hpp"
#include "types/abstractCreator.hpp"
#include "system/exception/registryException.hpp"


namespace cinite {
    namespace core {
        /** Registry subclasses */
        namespace registry {
            class Pack;
        }

        /** Global registry class */
        class Registry {
            public:
                /**
                 * Defined as singleton
                 * @see __CINITE_SINGLETON(CLASS)
                 */
                __CINITE_SINGLETON(Registry);

                /** type: abstract   */
                const static unsigned int t_abstract   = 0;

                /** type: model      */
                const static unsigned int t_model      = 1;

                /** type: controller */
                const static unsigned int t_controller = 2;

                /** type: view       */
                const static unsigned int t_view       = 3;

                /** type: driver     */
                const static unsigned int t_driver     = 4;

                /**
                 * Insert abstract to registry
                 * @param pack    Abstract's pack name
                 * @param name    Abstract's name
                 * @param creator Abstract's creator
                 * @param type    Abstrac's type
                 */
                static void registerAbstract(std::string pack, std::string name, abstractCreatorFunc creator, const unsigned int type);

                /**
                 * Returns model's creator
                 * @param  pack Model's pack name
                 * @param  name Model's name
                 * @return      Model's creator
                 */
                static abstractCreatorFunc getModelCreator(std::string pack, std::string name);

                /**
                 * Returns Controller's creator
                 * @param  pack Controller's pack name
                 * @param  name Controller's name
                 * @return      Controller's creator
                 */
                static abstractCreatorFunc getControllerCreator(std::string pack, std::string name);

                /**
                 * Returns view's creator
                 * @param  pack View's pack name
                 * @param  name View's name
                 * @return      View's creator
                 */
                static abstractCreatorFunc getViewCreator(std::string pack, std::string name);


                /**
                 * Returns driver's creator
                 * @param  pack Driver's pack name
                 * @param  name Driver's name
                 * @return      Driver's creator
                 */
                static abstractCreatorFunc getDriverCreator(std::string pack, std::string name);

            private:
                /**
                 * Get pack by name
                 * @param  name   Pack's name
                 * @param  create If true, this will create new pack if none found
                 * @return        Found or created pack
                 */
                registry::Pack *getPack(std::string name, bool create = false);

                /** Registered packs */
                std::map<std::string, registry::Pack*> _packs;
        };
    }
}

#endif
