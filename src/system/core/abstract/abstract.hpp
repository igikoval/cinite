#ifndef H_SYSTEM_CORE_ABSTRACT_ABSTRACT
#define H_SYSTEM_CORE_ABSTRACT_ABSTRACT

#include <map>
#include <string>
#include "../types/registerClass.hpp"

namespace cinite {
namespace core {

    class Config;

    /** Abstract */
    namespace abstract {
        class Model;
        class View;
        class Driver;

        /** Basic abstract class */
        class Abstract {

            public:
                /**
                 * Abstract constructor
                 * @param _loader Base engine loader that should be used
                 */
                Abstract(Loader &_loader): _loader(_loader){};

                /**
                 * Abstract virtual destructor
                 */
                virtual ~Abstract(){};

                /**
                 * Virtual function to init the class
                 *   This should be used in place of overriding constructor.
                 *   Method will be called instantly after creating object
                 */
                virtual void __init(){};

                /** type: Abstract */
                const static unsigned int __regType = Registry::t_abstract;

            protected:
                /** @see Loader::getModel */
                instance<Model> getModel(std::string pack, std::string name);

                /** @see Loader::getSingleton */
                instance<Model> getSingleton(std::string pack, std::string name);

                /** @see Loader::getView */
                instance<View> getView(std::string pack, std::string name);

                /** @see Loader::getDriver */
                instance<Driver> getDriver(std::string name, std::string inst = "");

                /** @see Loader::getConfig */
                Config *getConfig();


                /** @see Loader::getModel */
                template <class T>
                instance<T> getModel(std::string pack, std::string name) {
                    return cast<T>(this->getModel(pack, name));
                }

                /** @see Loader::getSingleton */
                template <class T>
                instance<T> getSingleton(std::string pack, std::string name) {
                    return cast<T>(this->getSingleton(pack, name));
                }

                /** @see Loader::getView */
                template <class T>
                instance<T> getView(std::string pack, std::string name) {
                    return cast<T>(this->getView(pack, name));
                }

                /** @see Loader::getDriver */
                template <class T>
                instance<T> getDriver(std::string name, std::string inst = "") {
                    return cast<T>(this->getDriver(name, inst));
                }

            private:
                /** Loader */
                Loader &_loader;
        };
    }
}
}

#endif
