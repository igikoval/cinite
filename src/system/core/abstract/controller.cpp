#include "controller.hpp"
#include "system/exception/coreException.hpp"

using namespace cinite;
using namespace cinite::core;
using namespace cinite::core::abstract;

Controller::method Controller::getMethod(std::string name) {
    std::map<std::string, Controller::method>::iterator it;

    it = this->_methods.find(name);

    if (it == this->_methods.end()) {
        throw exception::coreException(exception::coreException::NOT_VALID_METHOD, "Method not found (" + name + ")");
    }

    return it->second;
}

void Controller::registerMethod(std::string name, Controller::method method) {
    this->_methods[name] = method;
}
