#ifndef H_SYSTEM_CORE_ABSTRACT_DRIVER
#define H_SYSTEM_CORE_ABSTRACT_DRIVER

#include <map>
#include <string>
#include "abstract.hpp"

namespace cinite {
namespace core {
    /** Drivers */
    namespace driver {}

    namespace abstract {
        class Model;

        /** Abstract driver class */
        class Driver: public Abstract {

            public:
                /**
                 * Inherited abstract constructor
                 * @see Abstract::Abstract
                 */
                CINITE_ABSTRACT_CONSTRUCT();

                /** type: Driver */
                const static unsigned int __regType = Registry::t_driver;
        };
    }
}
}

#endif
