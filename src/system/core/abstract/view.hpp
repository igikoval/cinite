#ifndef H_SYSTEM_CORE_ABSTRACT_VIEW
#define H_SYSTEM_CORE_ABSTRACT_VIEW

#include <string>
#include "abstract.hpp"

namespace cinite {
namespace core {
    namespace abstract {

        /** Abstract view class */
        class View: public Abstract {
            public:
                /**
                 * Inherited abstract constructor
                 * @see Abstract::Abstract
                 */
                CINITE_ABSTRACT_CONSTRUCT();

                /** Returns view buffer */
                std::string getText();

                /** Prints view buffer to stdout */
                void printText();

                /** type: Model */
                const static unsigned int __regType = Registry::t_view;

            protected:
                /** Appends text to buffer */
                View& write(std::string text);

                /** @see View::write */
                View& operator<<(std::string text);

                /** Clears buffer */
                View& clear();

                /** Virtual method, used for parsing the buffer before geting contents */
                virtual std::string __parse(std::string buffer);

            private:
                /** Parses the buffer */
                void __parse();

                /** True if buffer was already parsed */
                bool __parsed = false;

                /** internal buffer */
                std::string _buffer = "";
        };
    }
}
}

#endif
