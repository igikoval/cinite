#ifndef H_SYSTEM_CORE_ABSTRACT_MODEL
#define H_SYSTEM_CORE_ABSTRACT_MODEL

#include <string>
#include "abstract.hpp"

namespace cinite {
namespace core {
    namespace abstract {

        /** Abstract model class */
        class Model: public Abstract {
            public:
                /**
                 * Inherited abstract constructor
                 * @see Abstract::Abstract
                 */
                CINITE_ABSTRACT_CONSTRUCT();

                /** type: Model */
                const static unsigned int __regType = Registry::t_model;
        };
    }
}
}

#endif
