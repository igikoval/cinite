#include <iostream>
#include "view.hpp"

using namespace cinite;
using namespace cinite::core;
using namespace cinite::core::abstract;

std::string View::getText() {
    this->__parse();
    return this->_buffer;
}

void View::printText() {
    this->__parse();
    std::cout << this->_buffer;
}

View& View::write(std::string text) {
    this->_buffer += text;

    return *this;
}

View& View::operator<<(std::string text) {
    return this->write(text);
}

View& View::clear() {
    this->_buffer = "";
}

std::string View::__parse(std::string buffer) {
    return buffer;
}

void View::__parse() {
    if (this->__parsed == true)
        return;

    this->_buffer = this->__parse(this->_buffer);
    this->__parsed = true;
}
