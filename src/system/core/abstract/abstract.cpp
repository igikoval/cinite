#include <string>
#include "abstract.hpp"
#include "../loader.hpp"

using namespace cinite;
using namespace cinite::core;
using namespace cinite::core::abstract;

instance<Model> Abstract::getModel(std::string pack, std::string name) {
    return this->_loader.getModel(pack, name);
}

instance<Model> Abstract::getSingleton(std::string pack, std::string name) {
    return this->_loader.getSingleton(pack, name);
}

instance<View> Abstract::getView(std::string pack, std::string name) {
    return this->_loader.getView(pack, name);
}

instance<Driver> Abstract::getDriver(std::string name, std::string inst) {
    return this->_loader.getDriver(name, inst);
}

Config *Abstract::getConfig() {
    return this->_loader.getConfig();
}
