#ifndef H_SYSTEM_CORE_ABSTRACT_CONTROLLER
#define H_SYSTEM_CORE_ABSTRACT_CONTROLLER

#include <string>
#include <vector>
#include <map>
#include "abstract.hpp"

namespace cinite {
namespace core {
    namespace abstract {

                /** Abstract cotroller class */
        class Controller: public Abstract {
            public:
                /**
                 * Inherited abstract constructor
                 * @see Abstract::Abstract
                 */
                CINITE_ABSTRACT_CONSTRUCT();

                /** Controller method type */
                typedef void(Controller::*method)(std::vector<std::string>);

                /** Return controller's method by name  */
                Controller::method getMethod(std::string name);

                /** type: controller */
                const static unsigned int __regType = Registry::t_controller;

            protected:
                /**
                 * Registers method
                 *  Should be called in init
                 * @param name   Method name
                 * @param method Method itself
                 */
                void registerMethod(std::string name, Controller::method method);

            private:
                /** Registered methods */
                std::map<std::string, Controller::method> _methods;
        };
    }
}
}

#endif
