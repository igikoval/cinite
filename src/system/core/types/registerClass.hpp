#ifndef H_SYSTEM_CORE_TYPE_REGISTERCLASS
#define H_SYSTEM_CORE_TYPE_REGISTERCLASS

#include <string>
#include "../registry.hpp"

namespace cinite {
namespace core {


    /**
     * Class registration structured
     *   used to register class in it's pack so it can be used later
     */
    template<typename T>
    struct registerClass {

        /**
         * Registering function
         * @param pack Pack name
         * @param name Class name
         */
        registerClass(const std::string pack, const std::string name) {
            Registry::registerAbstract(pack, name, (abstractCreatorFunc)abstractCreator<T>, T::__regType);
        }
    };

}
}

#endif
