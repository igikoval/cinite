#ifndef H_SYSTEM_CORE_TYPE_ABSTRACTCREATOR
#define H_SYSTEM_CORE_TYPE_ABSTRACTCREATOR

#include <memory>

namespace cinite {

    /**
     * Abstract instance type.
     *   Right now it uses just shared_ptr
     * @typedef instance
     */
    template <typename T>
    using instance = std::shared_ptr<T>;

    /**
     * Casting types.
     *   A short for static_pointer_cast.
     * @typedef cast(const std::shared_ptr< U > &sp)
     */
    template <class T, class U>
    instance<T> cast (const std::shared_ptr<U>& sp){
        return std::static_pointer_cast<T>(sp);
    }

    namespace core {

        class Loader;

        namespace abstract {
            class Abstract;
        }

        /**
         * Abstract creator type
         *   used for receiving constructor pointer to register a class
         * @typedef abstractCreator(Loader &_loader)
         */
        template <typename T>
        instance<abstract::Abstract> abstractCreator(Loader &_loader) {
            return std::make_shared<T>(_loader);
        }

        /**
         * Abstract creator function type
         *  used for calling abstractCreator
         * @typedef abstractCreatorFunc
         */
        typedef instance<abstract::Abstract> (*abstractCreatorFunc)(Loader &_loader);

    }
}

#endif
