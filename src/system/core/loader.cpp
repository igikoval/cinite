#include "loader.hpp"
#include <regex>
#include <iostream>
#include "abstract/abstract.hpp"
#include "abstract/model.hpp"
#include "abstract/view.hpp"
#include "abstract/driver.hpp"

using namespace cinite;
using namespace cinite::core;

Loader::~Loader() {

}

instance<abstract::Model> Loader::getModel(std::string pack, std::string name) {
    abstractCreatorFunc creator = Registry::getModelCreator(pack, name);
    instance<abstract::Model> model = std::static_pointer_cast<abstract::Model>(creator(*this));
    model->__init();

    return model;
}

instance<abstract::View> Loader::getView(std::string pack, std::string name) {
    abstractCreatorFunc creator = Registry::getViewCreator(pack, name);
    instance<abstract::View> view = std::static_pointer_cast<abstract::View>(creator(*this));
    view->__init();

    return view;
}

instance<abstract::Model> Loader::getSingleton(std::string pack, std::string name) {
    std::map<std::string, instance<abstract::Model>>::iterator it;
    std::string intname = this->_translateName(pack, name);

    it = this->_singletons.find(intname);

    if (it == this->_singletons.end()) {
        instance<abstract::Model> singleton = this->getModel(pack, name);
        this->_singletons[intname] = singleton;

        return singleton;
    }

    return it->second;
}

instance<abstract::Driver> Loader::getDriver(std::string name, std::string inst) {
    std::map<std::string, instance<abstract::Driver>>::iterator it;
    std::string intname = this->_translateName(name, inst);

    it = this->_drivers.find(intname);

    if (it == this->_drivers.end()) {

        abstractCreatorFunc creator = Registry::getDriverCreator("core", name);
        instance<abstract::Driver> driver = std::static_pointer_cast<abstract::Driver>(creator(*this));
        this->_drivers[intname] = driver;

        return driver;
    }

    return it->second;
}

void Loader::freeSingleton(std::string pack, std::string name) {
    std::map<std::string, instance<abstract::Model>>::iterator it;
    std::string intname = this->_translateName(pack, name);

    it = this->_singletons.find(intname);

    if (it != this->_singletons.end()) {
        this->_singletons.erase(intname);
    }
}

void Loader::freeDriver(std::string name, std::string inst) {
    std::map<std::string, instance<abstract::Driver>>::iterator it;
    std::string intname = this->_translateName(name, inst);

    it = this->_drivers.find(intname);

    if (it != this->_drivers.end()) {
        this->_drivers.erase(it);
    }
}

std::pair<std::string, std::string> Loader::_translateName(std::string wholename) {
    std::pair<std::string, std::string> ret;

    std::regex namesplit("([^/]*)/([^/]*)");

    std::smatch matches;
    if (std::regex_match(wholename, matches, namesplit)) {
        ret.first = matches[1];
        ret.second = matches[2];
    }

    return ret;

}

std::string Loader::_translateName(std::string pack, std::string name) {
    return pack + "/" + name;
}
