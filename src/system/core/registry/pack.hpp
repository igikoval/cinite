#ifndef H_SYSTEM_CORE_REGISTRY_PACK
#define H_SYSTEM_CORE_REGISTRY_PACK

#include <string>
#include <map>
#include "../types/abstractCreator.hpp"

namespace cinite {
namespace   core {
namespace     registry {

    /** Registry pack - contains models, creators, and other registered things */
    class Pack {
        public:
            /**
             * Blank pack constructor
             * @param name Pack's name
             */
            Pack(std::string name);

            /**
             * Returns pack's name
             */
            std::string getName();

            /**
             * Adds abstract to pack
             * @param name    Abstract's name
             * @param creator Abstract's creator
             * @param type    Abstract's type (see Registry for types)
             */
            void registerAbstract(std::string name, abstractCreatorFunc creator, const unsigned int type);

            /**
             * Gets model's creator
             * @param  name Model's name
             * @return      Creator
             */
            abstractCreatorFunc getModelCreator(std::string name);

            /**
             * Gets controller's creator
             * @param  name Controller's name
             * @return      Creator
             */
            abstractCreatorFunc getControllerCreator(std::string name);

            /**
             * Gets view's creator
             * @param  name View's name
             * @return      Creator
             */
            abstractCreatorFunc getViewCreator(std::string name);

            /**
             * Gets driver's creator
             * @param  name Driver's name
             * @return      Creator
             */
            abstractCreatorFunc getDriverCreator(std::string name);

        private:
            /** Pack's name */
            std::string name;

            /** Pack's models */
            std::map<std::string, abstractCreatorFunc> _models;

            /** Pack's controllers */
            std::map<std::string, abstractCreatorFunc> _controllers;

            /** Pack's views */
            std::map<std::string, abstractCreatorFunc> _views;

            /** Pack's drivers */
            std::map<std::string, abstractCreatorFunc> _drivers;
    };

}
}
}

#endif
