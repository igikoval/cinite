#include "pack.hpp"
#include "../registry.hpp"
#include "system/exception/registryException.hpp"

using namespace cinite::core;
using namespace cinite::core::registry;

Pack::Pack(std::string name) {
    this->name = name;
}

std::string Pack::getName() {
    return this->name;
}

void Pack::registerAbstract(std::string name, abstractCreatorFunc creator, const unsigned int type) {

    switch (type) {

        case Registry::t_model:
            this->_models[name] = creator;
            break;

        case Registry::t_controller:
            this->_controllers[name] = creator;
            break;

        case Registry::t_view:
            this->_views[name] = creator;
            break;

        case Registry::t_driver:
            this->_drivers[name] = creator;
            break;
    }

}

abstractCreatorFunc Pack::getModelCreator(std::string name) {
    std::map<std::string, abstractCreatorFunc>::iterator it;

    it = this->_models.find(name);

    if (it == this->_models.end()) {
        throw exception::registryException(exception::registryException::MODEL_NOT_REGISTERED, "Model not found in registry (" + name + ")");
    }

    return (abstractCreatorFunc)it->second;
}


abstractCreatorFunc Pack::getControllerCreator(std::string name) {
    std::map<std::string, abstractCreatorFunc>::iterator it;

    it = this->_controllers.find(name);

    if (it == this->_controllers.end()) {
        throw exception::registryException(exception::registryException::CONTROLLER_NOT_REGISTERED, "Controller not found in registry (" + name + ")");
    }

    return (abstractCreatorFunc)it->second;
}

abstractCreatorFunc Pack::getViewCreator(std::string name) {
    std::map<std::string, abstractCreatorFunc>::iterator it;

    it = this->_views.find(name);

    if (it == this->_views.end()) {
        throw exception::registryException(exception::registryException::VIEW_NOT_REGISTERED, "View not found in registry (" + name + ")");
    }

    return (abstractCreatorFunc)it->second;
}


abstractCreatorFunc Pack::getDriverCreator(std::string name) {
    std::map<std::string, abstractCreatorFunc>::iterator it;

    it = this->_drivers.find(name);

    if (it == this->_drivers.end()) {
        throw exception::registryException(exception::registryException::DRIVER_NOT_REGISTERED, "Driver not found in registry (" + name + ")");
    }

    return (abstractCreatorFunc)it->second;
}
