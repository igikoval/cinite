#ifndef H_SYSTEM_CORE_CONFIG
#define H_SYSTEM_CORE_CONFIG

#include <string>

namespace cinite {
    namespace core {

        /**
         * Config class
         *  right now it's just a stub for holding hard-coded data
         */
        class Config {
            public:
                /** Blank constructor */
                Config(){};

                /** Blank destructor */
                ~Config(){};

                /** view files directory */
                std::string views_dir = "app/views/";
        };
    }
}

#endif
