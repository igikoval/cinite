#ifndef H_SYSTEM_CORE_CINITE
#define H_SYSTEM_CORE_CINITE

#include <string>
#include <vector>
#include "types/registerClass.hpp"
#include "macros.hpp"
#include "registry.hpp"
#include "loader.hpp"
#include "abstract/controller.hpp"
#include "abstract/model.hpp"
#include "abstract/view.hpp"
#include "abstract/driver.hpp"

/** Base cinite namespace */
namespace cinite {

    /** Core namespace */
    namespace core {

        /**
         * Base engine class
         */
        class Cinite {
            public:
                /**
                 * Default constructor - just creating the object.
                 *  Not much is done here
                 */
                Cinite();

                /**
                 * Main function constructor
                 *  Running application after creation
                 *
                 * @param argc Arguments count (from main function)
                 * @param argv Array of arguments
                 */
                Cinite(int argc, char * argv[]);

                /**
                 * Vector arguments constructor
                 *  Running application after creation
                 *
                 * @param arguments Vector of arguments
                 */
                Cinite(std::vector<std::string> arguments);

                /**
                 * Empty destructor
                 */
                ~Cinite(){};

                /**
                 * Starts application using provided arguments
                 *  Performed automagically when using constructor
                 *  with arguments.
                 *
                 * @param arguments Vector of arguments
                 */
                void run(std::vector<std::string> arguments);

            private:
                /** Engine loader class */
                Loader loader;
        };
    }
}

#endif
