#ifndef H_SYSTEM_CORE_DRIVER_FILE
#define H_SYSTEM_CORE_DRIVER_FILE

#include <string>
#include <vector>
#include "../abstract/driver.hpp"

namespace cinite {
namespace core {

    namespace driver {

        /** Files operations class */
        class File: public abstract::Driver {

            public:
                /**
                 * Inherited driver constructor
                 * @see abstract::Driver::Driver
                 */
                CINITE_DRIVER_CONSTRUCT();

                /**
                 * Reads and returns contents of a file
                 * @param  path Path to the file
                 */
                std::string getFileContents(std::string path);

                /**
                 * @see File::getFileContents
                 */
                std::vector<std::string> getFileLines(std::string path);

                /**
                 * Writes text to file
                 * @param path     Path to the file
                 * @param contents Text to be written
                 */
                bool putFileContents(std::string path, std::string contents);

            private:
                /** Registry definition */
                CINITE_DEFINE_REGISTRY(File);
        };
    }
}
}

#endif
