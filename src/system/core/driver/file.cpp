#include <iostream>
#include <fstream>
#include "file.hpp"

using namespace cinite;
using namespace cinite::core;
using namespace cinite::core::driver;

CINITE_REGISTER_CORE(File, File);

std::string File::getFileContents(std::string path) {
    std::string contents;

    std::ifstream file(path);

    if (file.is_open()) {
        std::string line;

        while (std::getline(file, line)) {
            contents += line + "\n";
        }

        contents = contents.substr(0, contents.length()-1);
    }

    return contents;
}

std::vector<std::string> File::getFileLines(std::string path) {
    std::ifstream file(path);

    std::string line;
    std::vector<std::string> lines;

    if (file.is_open()) {
        while (std::getline(file, line)) {
            lines.push_back(line);
        }
    }

    return lines;
}

bool File::putFileContents(std::string path, std::string contents) {
    std::ofstream file(path);

    if (file.is_open()) {
        file << contents;
        file.close();

        return true;
    }

    return false;
}
