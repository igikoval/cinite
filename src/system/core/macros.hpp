#ifndef H_SYSTEM_CORE_MACROS
#define H_SYSTEM_CORE_MACROS

#define __CINITE_SINGLETON(CLASS)          \
    static CLASS& getInstance() {          \
        static CLASS instance;             \
        return instance;                   \
    }                                      \
    CLASS(){};                             \
    ~CLASS();                              \
    CLASS(CLASS const&)       = delete;    \
    void operator=(CLASS const&) = delete;

/**
 * Abstract's constructor
 * @def CINITE_ABSTRACT_CONSTRUCT()
 */
#define CINITE_ABSTRACT_CONSTRUCT() \
    using cinite::core::abstract::Abstract::Abstract

/**
 *  Model's constructor - should be public
 *  @def CINITE_MODEL_CONSTRUCT()
 */
#define CINITE_MODEL_CONSTRUCT() \
    using cinite::core::abstract::Model::Model

/**
 * Controller's constructor - should be public
 * @def CINITE_CONTROLLER_CONSTRUCT()
 */
#define CINITE_CONTROLLER_CONSTRUCT() \
    using cinite::core::abstract::Controller::Controller

/**
 * View's constructor - should be public
 * @def CINITE_VIEW_CONSTRUCT()
 */
#define CINITE_VIEW_CONSTRUCT() \
    using cinite::core::abstract::View::View

/**
 * Driver's constructor - should be public
 * @def CINITE_DRIVER_CONSTRUCT()
 */
#define CINITE_DRIVER_CONSTRUCT() \
    using cinite::core::abstract::Driver::Driver

/**
 * Defines registry class member
 *   should be added in private section
 * @def CINITE_DEFINE_REGISTRY(CLASS)
 * @param  CLASS Class name
 */
#define CINITE_DEFINE_REGISTRY(CLASS) \
    static cinite::core::registerClass<CLASS> __cinite_reg

/**
 * Push class to registry
 * @def CINITE_REGISTER(CLASS, PACK, NAME)
 * @param  CLASS Class name
 * @param  PACK  Pack name
 * @param  NAME  String name - this will be used for indexing in registry
 */
#define CINITE_REGISTER(CLASS, PACK, NAME) \
    cinite::core::registerClass<CLASS> CLASS::__cinite_reg(#PACK, #NAME)

/**
 * Push class to core registry pack
 * @def CINITE_REGISTER_CORE(CLASS, NAME)
 * @see CINITE_REGISTER(CLASS, PACK, NAME)
 */
#define CINITE_REGISTER_CORE(CLASS, NAME) \
    cinite::core::registerClass<CLASS> CLASS::__cinite_reg("core", #NAME)


/**
 * Adds method to controller registry
 *   should be called in controller's init
 * @param  CLASS Constructor class name (as in code)
 * @param  NAME  Method name
 */
#define CINITE_ADD_METHOD(CLASS, NAME) \
    this->registerMethod(#NAME, (cinite::core::abstract::Controller::method)&CLASS::NAME)

#endif
