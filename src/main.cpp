#include <iostream>
#include "config.hpp"
#include "system/core/cinite.hpp"
#include "system/exception/coreException.hpp"

int main(int argc, char * argv[]) {
    using namespace cinite::core;

    try {
        Cinite ngin = Cinite(argc, argv);

    } catch (cinite::exception::coreException ex) {
        std::cout << ex.message << std::endl;
    }

    return 0;
}
